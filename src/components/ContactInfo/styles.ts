import styled from 'styled-components';
import tw from 'twin.macro';

export const ContactInfoItem = styled.div`
  ${tw`my-3 flex flex-wrap w-full justify-center content-start items-stretch w-full items-center`};
`;
