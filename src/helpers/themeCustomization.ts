import { useStaticQuery, graphql } from 'gatsby';

interface GqlMdColorMap {
  edges: [
    {
      node: {
        id: string;
        frontmatter: {
          category: string;
          title: string;
          colors: [string];
        };
      };
    }
  ];
}

export const useBarColormap: () => [string] = () => {
  const { barColormapResults }: Record<string, GqlMdColorMap> = useStaticQuery(graphql`
    query {
      barColormapResults: allMarkdownRemark(filter: { frontmatter: { title: { eq: "Skillbars" } } }) {
        edges {
          node {
            frontmatter {
              colors
            }
          }
        }
      }
    }
  `);

  return barColormapResults.edges[0].node.frontmatter.colors;
};
