import React from 'react';

interface Props {
  content: React.ReactNode;
}

const FormatHtml: React.FC<Props> = ({ content }) => (
  <span
    className="format-html"
    dangerouslySetInnerHTML={{
      __html: content?.toString() as string
    }}
  />
);

export default FormatHtml;
