import styled from 'styled-components';
import tw, { theme } from 'twin.macro';

export interface BarProps {
  color: string;
  percentage: number;
}

export interface TitleProps {
  color: string;
}

export interface CustomColorHash {
  [key: string]: {
    bgColor: string;
    textColor: string;
  };
}

const nameToColors: CustomColorHash = {
  dark_sky_blue: { bgColor: theme`colors.dark_sky_blue`, textColor: theme`colors.black` },
  light_cyan: { bgColor: theme`colors.light_cyan`, textColor: theme`colors.black` },
  citrine: { bgColor: theme`colors.citrine`, textColor: theme`colors.black` },
  international_orange_engineering: {
    bgColor: theme`colors.international_orange_engineering`,
    textColor: theme`colors.white`
  },
  claret: { bgColor: theme`colors.claret`, textColor: theme`colors.black` }
};

export const ProgressBar = styled.div`
  ${tw`flex p-0 w-full justify-center items-center`};
`;

export const Bar = styled.div<BarProps>`
  ${tw`flex items-stretch justify-center items-center h-10 rounded-lg border-b-2 border-black align-middle cursor-default`};
  ${({ color, percentage }) => `width: ${percentage}%; background-color: ${nameToColors[color].bgColor}`};
`;

export const Content = styled.div`
  ${tw`w-full flex justify-between`};
`;

export const Title = styled.span<TitleProps>`
  ${tw`text-sm font-bold align-middle self-center`};
  ${({ color }) => `color: ${nameToColors[color].textColor}`};
`;

export const Percentage = styled.h2`
  ${tw`font-semibold`};
`;
