import React from 'react';
import { useStaticQuery, graphql } from 'gatsby';
import Img from 'gatsby-image';

import Container from 'components/ui/Container';
import TitleSection from 'components/ui/TitleSection';

import { SectionTitle, ImageSharpFluid } from 'helpers/definitions';

import * as Styled from './styles';

interface Certification {
  node: {
    frontmatter: {
      category: string;
      certName: string;
      issueDate: string;
      badge: {
        childImageSharp: {
          fluid: ImageSharpFluid;
        };
      };
    };
  };
}

const Certifications: React.FC = () => {
  const { markdownRemark, allMarkdownRemark } = useStaticQuery(graphql`
    query {
      markdownRemark(frontmatter: { category: { eq: "certifications section" } }) {
        frontmatter {
          title
          subtitle
        }
      }
      allMarkdownRemark(
        filter: { frontmatter: { category: { eq: "certifications" } } }
        sort: { fields: frontmatter___date, order: DESC }
      ) {
        edges {
          node {
            id
            frontmatter {
              certName
              issueDate
              badge {
                childImageSharp {
                  fluid(maxWidth: 180) {
                    ...GatsbyImageSharpFluid
                  }
                }
              }
            }
          }
        }
      }
    }
  `);

  markdownRemark;
  const sectionTitle: SectionTitle = markdownRemark.frontmatter;
  const certifications: Certification[] = allMarkdownRemark.edges;

  return (
    <Container section>
      <TitleSection title={sectionTitle.title} subtitle={sectionTitle.subtitle} center />
      {certifications.map((cert, i) => {
        const {
          frontmatter: { certName, badge }
        } = cert.node;
        return (
          <Styled.Image key={i}>
            <Img fluid={badge.childImageSharp.fluid} alt={certName} />
          </Styled.Image>
        );
      })}
    </Container>
  );
};

export default Certifications;
