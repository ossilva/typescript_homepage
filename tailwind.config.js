module.exports = {
  theme: {
    extend: {
      colors: {
        dark_sky_blue: '#7fb7beff',
        light_cyan: '#d3f3eeff',
        citrine: '#dacc3eff',
        international_orange_engineering: '#bc2c1aff',
        claret: '#7d1538ff'
      }
    }
  },
  variants: {},
  plugins: []
};
