import React from 'react';

import Container from 'components/ui/Container';

import * as Styled from './styles';

const Footer: React.FC = () => (
  <Styled.Footer>
    <Container>
      <Styled.Links>
        <Styled.Link href="https://gitlab.com/ossilva" rel="noreferrer noopener" target="_blank">
          GitLab
        </Styled.Link>
        <Styled.Link
          href="https://www.linkedin.com/in/oscar-silva-b1280a114/"
          rel="noreferrer noopener"
          target="_blank"
        >
          LinkedIn
        </Styled.Link>
      </Styled.Links>
    </Container>
  </Styled.Footer>
);

export default Footer;
