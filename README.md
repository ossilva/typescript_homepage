# Homepage source 

The source code for my homepage temporariliy in https://typescript-homepage.herokuapp.com/ without the page contents.

Credit for the design goes to Saimir Kapaj for his Gatsby Starter:
https://www.gatsbyjs.com/starters/SaimirKapaj/gatsby-markdown-typescript-personal-website

# Modifications to starter
1. Create certifications component
2. Add 'gatsby-remark-classes' plugin for styling blog contents
3. Replace outdated tailwind.macro with twin.macro
4. Create visualization of professional skills as stacks and labels.

# Used tech
- [Gatsby](https://www.gatsbyjs.org/)
- [Markdown](https://www.markdownguide.org/)
- [Typescript](https://www.typescriptlang.org/)
- [GraphQL](https://graphql.org/)
- [Styled Components](https://styled-components.com/)
- [Talwind CSS](https://tailwindcss.com/)
- [Framer Motion](https://www.framer.com/motion/)
- [React Font Awesome](https://github.com/FortAwesome/react-fontawesome/)
