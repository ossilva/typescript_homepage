import React from 'react';

import * as Styled from './styles';

interface Props extends Styled.BarProps {
  title: string;
}

const ProgressBar: React.FC<Props> = ({ title, percentage, color }) => (
  <Styled.ProgressBar>
    <Styled.Bar color={color} percentage={percentage}>
      <Styled.Title color={color}>{title}</Styled.Title>
    </Styled.Bar>
  </Styled.ProgressBar>
);

export default ProgressBar;
