import React from 'react';
import { useStaticQuery, graphql } from 'gatsby';

import Container from 'components/ui/Container';
import TitleSection from 'components/ui/TitleSection';
import SkillBar from 'components/ui/SkillBar';

import { SectionTitle } from 'helpers/definitions';
import { useBarColormap } from 'helpers/themeCustomization';

import * as Styled from './styles';

interface GqlMdSkill {
  node: {
    id: string;
    frontmatter: {
      skillset: string;
      title: string;
      percentage?: number;
    };
  };
}

interface Skill {
  _id: string;
  skillset: string;
  title: string;
  percentage: number;
}

type MinorSkill = Omit<Skill, 'percentage'>;

const flattenGQLSillData = (gqlSkill: GqlMdSkill): Skill | MinorSkill => {
  return { ...gqlSkill.node.frontmatter, _id: gqlSkill.node.id };
};

type SkillsetGroup = Record<Skill['skillset'], Skill[] | MinorSkill[]>;

// credit: SO user 'kevinrodriguez-io'
const groupBy = <T, K extends keyof unknown>(list: T[], getKey: (item: T) => K) =>
  list.reduce((previous, currentItem) => {
    const group = getKey(currentItem);
    if (!previous[group]) previous[group] = [];
    previous[group].push(currentItem);
    return previous;
  }, {} as Record<K, T[]>);

const outputLabels = (minorGroup: MinorSkill[]) => (
  <>
    {minorGroup.map((skill, i) => (
      <Styled.Label key={i}>{skill.title}</Styled.Label>
    ))}
  </>
);

const Skills: React.FC = () => {
  const { markdownRemark, majorSkillsRemark, minorSkillsRemark } = useStaticQuery(graphql`
    fragment Skill on MarkdownRemarkConnection {
      edges {
        node {
          id
          frontmatter {
            skillset
            title
            percentage
          }
        }
      }
    }
    query {
      markdownRemark(frontmatter: { category: { eq: "skills section" } }) {
        frontmatter {
          title
          subtitle
        }
      }
      majorSkillsRemark: allMarkdownRemark(
        filter: { frontmatter: { category: { eq: "skills" } } }
        sort: { fields: fileAbsolutePath }
      ) {
        ...Skill
      }
      minorSkillsRemark: allMarkdownRemark(
        filter: { frontmatter: { category: { eq: "minor skills" } } }
        sort: { fields: fileAbsolutePath }
      ) {
        ...Skill
      }
    }
  `);

  const sectionTitle: SectionTitle = markdownRemark.frontmatter;
  const skillKindsRaw: GqlMdSkill[][] = [minorSkillsRemark.edges, majorSkillsRemark.edges];
  const skillKinds: (Skill | MinorSkill)[][] = skillKindsRaw.map((rawSkills) => rawSkills.map(flattenGQLSillData));
  const skillKindGroups: SkillsetGroup[] = skillKinds.map((skills) => groupBy(skills, (s) => s.skillset));
  const [minorGroups, majorGroups] = skillKindGroups;

  return (
    <Container section>
      <TitleSection title={sectionTitle.title} subtitle={sectionTitle.subtitle} center />
      <Styled.Skills>
        {Object.entries(majorGroups).map((entry, i) => {
          const barColors = useBarColormap();
          const [skillset, skills] = entry as [string, Skill[]];
          const sortedSkills = skills.sort((a, b) => a.percentage - b.percentage);
          const minorSkills = (minorGroups[skillset] || []) as MinorSkill[];
          return (
            <Styled.Skillset key={i}>
              <Styled.SkillsetTitle>{skillset}</Styled.SkillsetTitle>
              <Styled.Skillstack>
                {sortedSkills.map((item) => {
                  const { title, percentage } = item;
                  return (
                    <Styled.Skill key={item._id}>
                      <SkillBar title={title} percentage={percentage} color={barColors[i % barColors.length]} />
                    </Styled.Skill>
                  );
                })}
              </Styled.Skillstack>
              <Styled.Labelset>{outputLabels(minorSkills)}</Styled.Labelset>
            </Styled.Skillset>
          );
        })}
      </Styled.Skills>
    </Container>
  );
};

export default Skills;
