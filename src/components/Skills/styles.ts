import styled from 'styled-components';
import tw from 'twin.macro';

export const Skills = styled.div`
  ${tw`flex flex-wrap w-full h-full my-1`};
`;

export const Skillset = styled.div`
  ${tw`flex flex-wrap sm:w-1/2 justify-center p-2`};
`;

export const Skillstack = styled.div`
  ${tw`flex content-end flex-wrap h-40 w-full`};
`;

export const Labelset = styled.div`
  ${tw`my-3 h-20 flex flex-wrap w-full justify-center content-start`}
`;

export const Label = styled.span`
  ${tw`m-1 bg-gray-200 rounded-full px-2 font-bold text-sm leading-loose cursor-default border-black border-b-2`}
`;

export const Skill = styled.div`
  ${tw`w-full h-10`};
`;

export const SkillsetTitle = styled.h3`
  ${tw`font-bold text-lg place-self-start`};
`;
